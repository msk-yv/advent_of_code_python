import time

# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0 #change type if need

#TODO Part 1

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

# part 2
start = time.time()
answer = 0 #change type if need

#TODO Part 2

print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")

inp.close() 