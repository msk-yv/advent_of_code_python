import string, time

def get_next_string(s):
    if chr(ord(s[-1])+1) in string.ascii_lowercase:
        s = s[:-1] + chr(ord(s[-1])+1)
    else:
        s = get_next_string(s[:-1]) + 'a' 
    return s

def has_triplet(s):
    for i in range(len(s)-2):
        if s[i+1] == chr(ord(s[i])+1) and s[i+2] == chr(ord(s[i])+2):
            return True
    return False

def double_count(s):
    count = 0
    for i in string.ascii_lowercase:
        if (i + i) in s:
            count += 1
    return count

def find_next_password(s):
    while s < 'zzzzzzzz':
        s = get_next_string(s)
        if 'i' in s or 'o' in s or 'l' in s:
            continue
        if has_triplet(s) and double_count(s) > 1:
            return s
    
inp = 'hxbxwxba'

start = time.time()
answer = find_next_password(inp)

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

# part 2
start = time.time()

answer = find_next_password(answer)

print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")
