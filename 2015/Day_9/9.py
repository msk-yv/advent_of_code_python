import time

# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0 #change type if need

#TODO Part 1
distances = {}
for s in inp:
    town1, _, town2, _, d = s.split('\n')[0].split(' ')
    print(town1, '<->', town2, ':' , d)
    if distances.get(town1) is not None:
        distances[town1][town2] = int(d)
    else:
        distances[town1] = { town2: int(d)}

    if distances.get(town2) is not None:
        distances[town2][town1] = int(d)
    else:
        distances[town2] = { town1: int(d)}

print(distances)

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

# part 2
start = time.time()
answer = 0 #change type if need

#TODO Part 2

print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")

inp.close() 