import time

def set_address(x,y):
    return str(x)+","+str(y)

take_direction = {
    '<': [-1, 0],
    '>': [1, 0],
    '^': [0, 1],
    'v': [0, -1]
}

# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0 #change type if need

#TODO Part 1

x, y = 0, 0
addres_list = []
addres_list.append(set_address(x,y))

for s in inp:
    for c in s:
        x += take_direction[c][0]
        y += take_direction[c][1]
        addres_list.append(set_address(x,y))

answer = len(list(set(addres_list)))

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

inp.close() 

# part 2
inp = open("input.txt", "r")

start = time.time()
answer = 0 #change type if need

#TODO Part 2
santaX = 0
santaY = 0
roboSX = 0
roboSY = 0
addres_list2 = []
addres_list2.append(set_address(santaX,santaY))
for s in inp:
    for idx, c in enumerate(s):
        if idx % 2 == 0:
            santaX += take_direction[c][0]
            santaY += take_direction[c][1]
            addres_list2.append(set_address(santaX,santaY))
        else:
            roboSX += take_direction[c][0]
            roboSY += take_direction[c][1]
            addres_list2.append(set_address(roboSX,roboSY))

answer = len(list(set(addres_list2)))

print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")

 
inp.close() 