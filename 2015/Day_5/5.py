import time

def checK_vowels(s):
    vowelsanswer = 0
    for c in 'aeiou':
        vowelsanswer += s.count(c)
    return vowelsanswer > 2

def check_doubles(s):
    for c in 'abcdefghijklmnopqrstuvwxyz':
        if c * 2 in s:
            return True
    return False

def check_bad_pairs(s):
    for p in ['ab', 'cd', 'pq', 'xy']:
        if p in s:
            return False
    return True

def checK_doubles_pairs(s):
    i = 0
    while i < (len(s) - 1):
        pair = s[i:i+2]
        if pair in s[i+2:]:
            return True
        i += 1
    return False

def check_triplets(s):
    i = 0
    while i < (len(s) - 2):
        triplet = s[i:i+3]
        if triplet[0] == triplet [2]:
            return True
        i += 1
    return False

# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0 #change type if need

#TODO Part 1

for s in inp:
    if check_doubles(s) and checK_vowels(s) and check_bad_pairs(s):
        answer += 1
 
print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

inp.close() 
# part 2
inp = open("input.txt", "r")

start = time.time()
answer = 0 #change type if need

#TODO Part 2

for s in inp:
    if check_triplets(s) and checK_doubles_pairs(s):
        answer += 1
        
print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")

inp.close() 