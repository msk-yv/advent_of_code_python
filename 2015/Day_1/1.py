import time

def str_len_without_symbol(s, sym):
    return len(''.join(s.split(sym)))
    
# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0 

#TODO Part 1

inp_str = inp.readline()

answer = inp_str.count('(') - inp_str.count(')')

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

# part 2
start = time.time()
answer = 0 #change type if need

#TODO Part 2
current_floor = 0

while answer < len(inp_str) and current_floor != -1 :
    if inp_str[answer] == '(':
        current_floor += 1
    else:
        current_floor -= 1
    answer += 1

print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")

 
inp.close() 