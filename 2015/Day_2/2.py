import time

# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0
answer2 = 0

for s in  inp:
    x, y, z = sorted(map(int, s.split('x')))
    #TODO Part 1
    answer += 3 * x * y + 2 * x * z + 2 * y * z
    #TODO Part 2
    answer2 += 2 * x + 2 * y + x * y * z 

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

print("Part 2 answer: ", answer2)
print("Time to Part 2 answer: ", time.time() - start , " sec")

 
inp.close() 