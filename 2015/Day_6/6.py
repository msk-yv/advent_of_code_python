import time

def toggle(s_i, s_j, f_i, f_j):
    for i in range(s_i, f_i + 1):
        for j in range(s_j, f_j + 1):
            a[i][j] = 1 if a[i][j] == 0 else 0

def switch(s_i, s_j, f_i, f_j, q):
    for i in range(s_i, f_i + 1):
        for j in range(s_j, f_j + 1):
            a[i][j] = q

def toggle2(s_i, s_j, f_i, f_j):
    for i in range(s_i, f_i + 1):
        for j in range(s_j, f_j + 1):
            b[i][j] += 2

def switch2(s_i, s_j, f_i, f_j, q):
    for i in range(s_i, f_i + 1):
        for j in range(s_j, f_j + 1):
            b[i][j] = 0 if b[i][j]+q < 0 else b[i][j]+q


# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0 

#TODO Part 1

global a 
a = [ [0 for i in range(1000) ] for j in range(1000)]

for s in inp:
    answer = 0
    start_i = int(s.split(' through ')[0].split(',')[0].split(' ')[-1])
    start_j = int(s.split(' through ')[0].split(',')[1])
    finish_i = int(s.split(' through ')[1].split(',')[0])
    finish_j = int(s.split(' through ')[1].split(',')[1])
    helper = s.split(' ')
    if helper[0] == 'toggle':
        toggle(start_i, start_j, finish_i, finish_j)
    elif helper[1] == 'on':
        switch(start_i, start_j, finish_i, finish_j, 1)
    else:
        switch(start_i, start_j, finish_i, finish_j, 0) 

for i in range(1000):
    for j in range(1000):
        answer += a[i][j] 

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

inp.close()
# part 2
inp = open("input.txt", "r")
start = time.time()
answer = 0 #change type if need

#TODO Part 2

global b 
b = [ [0 for i in range(1000) ] for j in range(1000)]

for s in inp:
    answer = 0
    start_i = int(s.split(' through ')[0].split(',')[0].split(' ')[-1])
    start_j = int(s.split(' through ')[0].split(',')[1])
    finish_i = int(s.split(' through ')[1].split(',')[0])
    finish_j = int(s.split(' through ')[1].split(',')[1])
    helper = s.split(' ')
    if helper[0] == 'toggle':
        toggle2(start_i, start_j, finish_i, finish_j)
    elif helper[1] == 'on':
        switch2(start_i, start_j, finish_i, finish_j, 1)
    else:
        switch2(start_i, start_j, finish_i, finish_j, -1) 

for i in range(1000):
    for j in range(1000):
        answer += b[i][j] 

print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")

inp.close() 