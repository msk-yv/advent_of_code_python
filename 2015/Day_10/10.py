import time

def form_new_string(s):
    answ =''
    i = 0
    while i < len(inp):
        c = inp[i]
        j = i + 1
        while j < len(inp) and inp[j] == c:
            j += 1
        l = j - i
        answ += str(l) + c
        i += l
    return answ

# input for task
inp = '1113122113'

# part 1
start = time.time()
answer = 0 #change type if need

#TODO Part 1
for i in range(40):
    answer = form_new_string(inp)
    inp = answer

print("Part 1 answer: ", len(answer))
print("Time to Part 1 answer: ", time.time() - start , " sec")

# part 2
start = time.time()

#TODO Part 2
for i in range(10):
    answer = form_new_string(inp)
    inp = answer
print("Part 2 answer: ", len(answer))
print("Time to Part 2 answer: ", time.time() - start , " sec")
