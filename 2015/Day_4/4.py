import time
import hashlib

# input for task
inp = 'iwrupvqb'

# part 1
start = time.time()
answer = 1

#TODO Part 1

m = hashlib.md5()
m.update((inp+str(answer)).encode('utf-8'))
s = m.hexdigest()

while s[0:5] != '00000':
    answer += 1
    m = hashlib.md5()
    m.update((inp+str(answer)).encode('utf-8'))
    s = m.hexdigest()

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

# part 2
start = time.time()
answer = 1

#TODO Part 2
m = hashlib.md5()
m.update((inp+str(answer)).encode('utf-8'))
s = m.hexdigest()

while s[0:6] != '000000':
    answer += 1
    m = hashlib.md5()
    m.update((inp+str(answer)).encode('utf-8'))
    s = m.hexdigest()

print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")
