import time

def try_int_it(s):
    try:
        i = int(s)
        return True    
    except ValueError:
        return False
        
def evaluate(s):
    if values.get(s) is not None:
        return int(values[s])
    elif try_int_it(s):
        return int(s)
    else:
        a = circuit[s].split(' ')
        if len(a) == 1:
            values[s] = evaluate(a[0])
            return values[s]
        elif a[0] == 'NOT':
            values[s] = 2**16 + ~(evaluate(a[1]))
            return values[s]
        else:
            if a[1] == 'RSHIFT':
                values[s] = int(evaluate(a[0])) >> int(a[2])
                return values[s]
            elif a[1] == 'LSHIFT':
                values[s] = int(evaluate(a[0])) << int(a[2])
                return values[s]
            elif a[1] == 'OR':
                values[s] = int(evaluate(a[0])) | int(evaluate(a[2]))
                return values[s]
            elif a[1] == 'AND':
                values[s] = int(evaluate(a[0])) & int(evaluate(a[2]))
                return values[s]
                 
# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0 #change type if need

#TODO Part 1
global circuit
global values
circuit = {}
values = {}

for s in inp:
    target = s.split(' -> ')[1][:-1]
    source = s.split(' -> ')[0]
    # print(s, 'target: ', target, "source: ", source)
    if try_int_it(source):
        values[target] = int(source)
    circuit[target] = source

answer = evaluate('a')

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

# part 2
start = time.time()

#TODO Part 2
values = {}
values['b'] = answer
values['c'] = 0
answer = evaluate('a')

print("Part 2 answer: ", answer)
print("Time to Part 2 answer: ", time.time() - start , " sec")

inp.close() 