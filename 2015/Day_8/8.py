import time

# input for task
inp = open("input.txt", "r")

# part 1
start = time.time()
answer = 0 #change type if need
answer2 = 0
#TODO Part 1 && 2

for s in inp:
    code_chars_count = len(s)-1
    sym_chars = 0
    i = 1
    while i < len(s) - 2:
        if s[i] != "\\":
            sym_chars += 1
            i += 1
        elif s[i+1] == 'x':
            sym_chars += 1
            i += 4
        else:
            sym_chars += 1
            i += 2
    i = 0
    new_s = '"'
    while i < len(s) - 1:
        if s[i] == '"':
            new_s += '\\"'
            
        elif s[i] == "\\":
            new_s += '\\\\'
            
        else:
            new_s += s[i]
        i += 1
    new_s += '"'
    print(s, new_s)
    answer += code_chars_count - sym_chars
    answer2 += len(new_s) - code_chars_count 

print("Part 1 answer: ", answer)
print("Time to Part 1 answer: ", time.time() - start , " sec")

print("Part 2 answer: ", answer2)
print("Time to Part 2 answer: ", time.time() - start , " sec")

inp.close() 